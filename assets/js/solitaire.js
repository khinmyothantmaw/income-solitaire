//Client: NTUC Income Solitaire
//Developed by: Convertium Pte Ltd
$(document).ready(function() {
  $('.dropdown').on('shown.bs.dropdown', function(){
    $(this).parent().find(".fa-angle-down").removeClass("fa-angle-down").addClass("fa-angle-up");
  }).on('hidden.bs.dropdown', function(){
    $(this).parent().find(".fa-angle-up").removeClass("fa-angle-up").addClass("fa-angle-down");
  });

  // Mobile Navigation
  mobileNav();

  // Series Match Height
  $('.series-items p').matchHeight();

  // Animate when in View
  animationInView();

  // set full screen masthead
  fullScreenMasthead();

  // Carousel Function
  slickFunc();

  // Hover Carousel
  hoverCarousel();

  $('.collapse').on('shown.bs.collapse', function(){
    $(this).parent().find(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
    }).on('hidden.bs.collapse', function(){
    $(this).parent().find(".fa-minus").removeClass("fa-minus").addClass("fa-plus");
  });
});

$(window).resize(function() {
  // set full screen masthead
  fullScreenMasthead();

  // Carousel Function
  slickFunc();

});

function isMobile() {
  return (/Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) ? true : false;
}

function animationInView() {
  var $elements = $('.animation-element');
  // if ($('.solitaire-wrapper').length > 0)
  if ($(window).width() < 992) {
    $($elements).each(function(i, element) {
      new Waypoint({
        element: $(element),
        handler: function() {
          $(element).addClass('in-view');
        },
        offset: function() {
          return Waypoint.viewportHeight() - 100;
        }
      });
    });
  }
  $('.solitaire-wrapper.animation-element, .solitaire-wrapper .animation-element, .solitaire-inner.animation-element, .solitaire-inner .animation-element').each(function(i, element) {
    new Waypoint({
      element: $(element),
      handler: function() {
        $(element).addClass('in-view');
      },
      offset: function() {
        return Waypoint.viewportHeight() - 200;
      }
    });
  });
  $(window).scroll(function(){
    $($elements).each(function(i, element) {
        new Waypoint({
          element: $(element),
          handler: function() {
            $(element).addClass('in-view');
          },
          offset: function() {
            return Waypoint.viewportHeight() - 200;
          }
        });
      });
  });
  
}

// Mobile Navigation Body Overlay
$('#mobile-nav-right, .mobile-nav-body-overlay').click(function(){
  if($('#mobile-nav').hasClass('ps-active-panel')) {
    $('.mobile-nav-body-overlay').hide();
    $('body').css({ 'position': 'inherit' });
  } else {
    $('.mobile-nav-body-overlay').show();
    $('body').css({ 'position': 'fixed' });
  }
});

function mobileNav() {
  $('#mobile-nav-right').panelslider({side: 'right', duration: 300});

  $('#mobile-nav > ul > li:has(ul)').addClass("has-sub");

  $('#mobile-nav > ul > li > a').click(function() {
    var checkElement = $(this).next();

    $('#mobile-nav li').removeClass('active');
    $(this).closest('li').addClass('active');

    if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
      $(this).closest('li').removeClass('active');
      checkElement.slideUp('normal');
    }

    if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
      $('#mobile-nav ul ul:visible').slideUp('normal');
      checkElement.slideDown('normal');
    }

    if (checkElement.is('ul')) {
      return false;
    } else {
      return true;
    }
  });

  // Mobile Menu
  $(window).load(function() {
  	var heightArray = $("#mobile-nav.mobile-menu").map( function(){
  		return $(this).height();
  	}).get();
  	var maxHeight = Math.max.apply( Math, heightArray);
  	$(".mobile-menu-shadow").height(maxHeight);
  });
}

function fullScreenMasthead() { 
  if ($(window).width() >= 992) {
    $('.solitaire-wrapper').css("height", $(window).height() + "px");
    
  } 
  else {
    $('.solitaire-wrapper').css("height", "auto");

  }
}

function hoverCarousel() {
  $('.wrapper-hover a').hover(function () {
    $(this).parent().parent().addClass('hover');
  }, function () {
    $(this).parent().parent().removeClass('hover');
  });
}

function slickFunc(){
  if ($('.premier-plan').length > 0){
    if ($(window).width() > 768) {
      $('.premier-plan').slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        autoplay: true
      });
      
    }
    else {
      if($(".premier-plan .premier-item").length > 2) {
        $('.premier-plan').slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          autoplay: true,
          dots: true,
          arrows: false
        });
      }
    }
  }

  if($('.priviledges-treat').length > 0) {
    if ($(window).width() > 768) {
      $('.priviledges-treat').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        arrows: true
      });
      
    }
    else {
      if($(".premier-plan .premier-item").length > 2) {
        $('.priviledges-treat').slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          autoplay: true,
          dots: true,
          arrows: false
        });
      }
    }
  }
 
}